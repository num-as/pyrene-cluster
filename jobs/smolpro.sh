#!/bin/bash
#
#### MOLPRO JOB (MPI) ################
#
#  Usage: sbatch smolpro.sh input_file
#
######################################
#
#
#
########################################### BEGIN SLURM OPTIONS  ##################################
#
#
# JOB AND OUTPUT FILE NAMES. Uncomment if needed.
#   If not specified, the default output file name will be slurm-NNNN.out, 
#    where NNNN is the Slurm job id.
##SBATCH --job-name=NAME-TEST
##SBATCH --output=output_slurm.out
#
# MAIL NOTIFICATIONS (at job start, step, end or failure). Uncomment if needed.
##SBATCH --mail-type=ALL
##SBATCH --mail-user=firstname.lastname@univ-pau.fr
#
# EXECUTION IN THE CURRENT DIRECTORY
#SBATCH --workdir=.
#
# PARTITION
# The following command provides the state of available partitions:
#   sinfo
#SBATCH --partition=standard
#
# ACCOUNT
# The following command provides the list of accounts you can use:
#   sacctmgr list user withassoc name=your_username format=user,account,defaultaccount
#SBATCH --account=uppa
#
# JOB MAXIMAL WALLTIME. Format: D-H:M:S
#SBATCH --time=0-0:10:00
#
# CORES NUMBER 
#SBATCH --ntasks=<specify here the number of cores>
#SBATCH --cpus-per-task=1
#
# MEMORY PER CORE (MB)
# Molpro: 1 MegaWord (1 MW) =  8 MB
#  For a calculation of 100 MW = 800 MB on 4 Molpro tasks: 
# - each task will use 800 MW (except the worker, maybe),
# - the needed max memory is about: 800*4 = 3200 MB
#SBATCH --mem-per-cpu=800
#
#
########################################### END SLURM OPTIONS ##################################
#
#
#
######################## BEGIN UNIX COMMANDS  #########################

# MODULES
module purge
module load molpro/2015.1

# SCRATCH DIRECTORY
export TMPDIR=$SCRATCHDIR

# EXECUTION
#  Usage: molpro input_file
time molpro $1

######################### END UNIX COMMANDS #########################
