#!/bin/bash
#
#### PARALLEL JOB (DISTRIBUTED MEMORY WITH OPEN MPI) ####
#
#  Usage: sbatch spar_distrib_openmpi.sh
#
#########################################################
#
#
#
########################################### BEGIN SLURM OPTIONS  ##################################
#
#
# JOB AND OUTPUT FILE NAMES. Uncomment if needed.
#   If not specified, the default output file name will be slurm-NNNN.out, 
#    where NNNN is the Slurm job id.
##SBATCH --job-name=NAME-TEST
##SBATCH --output=output_slurm.out
#
# MAIL NOTIFICATIONS (at job start, step, end or failure). Uncomment if needed.
##SBATCH --mail-type=ALL
##SBATCH --mail-user=firstname.lastname@univ-pau.fr
#
# EXECUTION IN THE CURRENT DIRECTORY
#SBATCH --workdir=.
#
# PARTITION
# The following command provides the state of available partitions:
#   sinfo
#SBATCH --partition=standard
#
# ACCOUNT
# The following command provides the list of accounts you can use:
#   sacctmgr list user withassoc name=your_username format=user,account,defaultaccount
#SBATCH --account=uppa
#
# JOB MAXIMAL WALLTIME. Format: D-H:M:S
#SBATCH --time=0-0:10:00
#
# CORES NUMBER 
#SBATCH --ntasks=<specify here the number of cores>
#SBATCH --cpus-per-task=1
#
# MEMORY PER CORE (MB)
#SBATCH --mem-per-cpu=1000
#
#
########################################### END SLURM OPTIONS ##################################
#
#
#
######################## BEGIN UNIX COMMANDS  #########################

# MODULES
module purge
module load openmpi/3.1.4/gcc/8.3.0
module list

# COMMANDS
echo "------------------------------------------------------------"
type mpiexec
echo "SCRATCHDIR           =  $SCRATCHDIR"
echo "SLURM_JOB_NAME       =  $SLURM_JOB_NAME"
echo "SLURM_JOB_ID         =  $SLURM_JOB_ID"
echo "SLURM_JOB_NODELIST   =  $SLURM_JOB_NODELIST"
echo ""
echo "SLURM_JOB_NUM_NODES  =  $SLURM_JOB_NUM_NODES"
echo "SLURM_NTASKS         =  $SLURM_NTASKS"
echo "SLURM_TASKS_PER_NODE =  $SLURM_TASKS_PER_NODE"
echo "SLURM_CPUS_PER_TASK  =  $SLURM_CPUS_PER_TASK"
echo "------------------------------------------------------------"

# EXECUTION in batch mode with the mpiexec command
#  The executable must be in the PATH or in a absolute/relative path
#  Open MPI automatically detects the number of cores reserved by Slurm,
#   therefore it is not necessary to use the "-n" option:
mpiexec ./myParDistribMemoryApp.exe

# REMARK: in interactive mode, one must specify the number of MPI processes with the "-n" option:
#    mpiexec -n 4 ./myParDistribMemoryApp.exe


######################### END UNIX COMMANDS #########################
