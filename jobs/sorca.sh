#!/bin/bash

####### ORCA JOB #######
#
#  Usage: sbatch sorca.sh orca_input.inp
#
########################


#SBATCH --account=uppa
#SBATCH --partition=short
#SBATCH --workdir=.

#SBATCH --time=0-01:00:0

#SBATCH --nodes=1
#SBATCH --ntasks=4
#SBATCH --cpus-per-task=1

#SBATCH --mem-per-cpu=2000MB

module purge
module load orca/4.1.2
module list

echo "------------------------------------------------------------"
echo "SLURM_JOB_NAME       =" $SLURM_JOB_NAME
echo "SLURM_JOB_ID         =" $SLURM_JOB_ID
echo "SLURM_NODELIST       = "$SLURM_NODELIST
echo ""
echo "SLURM_JOB_NUM_NODES  =" $SLURM_JOB_NUM_NODES
echo "SLURM_NTASKS         =" $SLURM_NTASKS
echo "SLURM_TASKS_PER_NODE =" $SLURM_TASKS_PER_NODE
echo "SLURM_CPUS_PER_TASK  =" $SLURM_CPUS_PER_TASK
echo "------------------------------------------------------------"

# run orca
cp -v ${1%.*}.* $SCRATCHDIR
cp -v ${1%.*}.xyz ${1%.*}_old.xyz
cd $SCRATCHDIR
$(which orca) $1
cp -v * $SLURM_SUBMIT_DIR/
cd $SLURM_SUBMIT_DIR
mv -v slurm-$SLURM_JOB_ID.out ${1%.*}.out
